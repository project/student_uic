<?php

namespace Drupal\student_uic\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the 'student_uic' field type.
 *
 * @FieldType(
 *   id = "student_uic",
 *   label = @Translation("Student UIC"),
 *   category = @Translation("General"),
 *   default_widget = "student_uic",
 *   default_formatter = "student_uic_default"
 * )
 */
class StudentUICItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    $settings = [
      'maximum_length' => 255,
      'minimum_length' => NULL,
      'pattern' => [
        'regex' => 0,
        'options' => [
          'numbers' => 1,
        ],
        'allowed_characters' => '!@#$%^&*()\'',
      ],
    ];

    return $settings + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $settings = $this->getSettings();

    $element['minimum_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum length'),
      '#description' => $this->t('The minimum length of the field in characters. Leave blank to not enforce a minimum character length.'),
      '#default_value' => $settings['minimum_length'],
    ];

    $element['maximum_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum length'),
      '#description' => $this->t('The maximum length of the field in characters.'),
      '#default_value' => $settings['maximum_length'],
      '#required' => TRUE,
    ];

    $element['pattern'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Pattern'),
      '#description' => $this->t('The pattern determines what validation is ran when attempting to enter a value for this field.'),
    ];

    $element['pattern']['regex'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Field matches a regular expression'),
      '#default_value' => $settings['pattern']['regex'],
    ];

    $element['pattern']['regex_pattern'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Regular Expression Pattern'),
      '#description' => $this->t('Standard PHP regular expression patterns accepted. Regular expression will overwrite the above options.'),
      '#default_value' => in_array('regex_pattern', $settings['pattern']) ? $settings['pattern']['regex_pattern'] : NULL,
      '#states' => [
        'visible' => [
          ':input[name="settings[pattern][regex]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="settings[pattern][regex]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $element['pattern']['options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Validation Options'),
      '#options' => [
        'numeric' => $this->t('Numbers allowed'),
        'alpha' => $this->t('Alphabetical characters allowed'),
        'special' => $this->t('Special characters allowed'),
      ],
      '#states' => [
        'visible' => [
          ':input[name="settings[pattern][regex]"]' => ['checked' => FALSE],
        ],
        'required' => [
          ':input[name="settings[pattern][regex]"]' => ['checked' => FALSE],
        ],
      ],
      '#weight' => 1,
    ];

    $element['pattern']['allowed_characters'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Allowed special characters'),
      '#description' => $this->t('Enter the special characters allowed in the field.'),
      '#default_value' => $settings['pattern']['allowed_characters'],
      '#states' => [
        'visible' => [
          ':input[name="settings[pattern][options][special]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="settings[pattern][options][special]"]' => ['checked' => TRUE],
        ],
      ],
      '#weight' => 2,
    ];

    foreach ($element['pattern']['options']['#options'] as $key => $value) {
      if ($settings['pattern']['options'][$key] == 0) {
        unset($settings['pattern']['options'][$key]);
      }
    }

    $element['pattern']['options']['#default_value'] = $settings['pattern']['options'];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $student_uic = $this->get('student_uic')->getValue();

    return empty($student_uic);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['student_uic'] = DataDefinition::create('string')
      ->setLabel(t('Student UIC'))
      ->setDescription(t('The Student UIC'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $settings = $field_definition->getSettings();

    return [
      'columns' => [
        'student_uic' => [
          'type' => 'varchar',
          'length' => $settings['maximum_length'],
        ],
      ],
    ];
  }

}
