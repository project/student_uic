<?php

namespace Drupal\student_uic\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the 'student_uic' field widget.
 *
 * @FieldWidget(
 *   id = "student_uic",
 *   label = @Translation("Student UIC"),
 *   field_types = {"student_uic"},
 * )
 */
class StudentUICWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element['student_uic'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Student UIC'),
      '#title_display' => 'hidden',
      '#default_value' => $items[$delta]->student_uic ?? $items[$delta]->student_uic,
      '#size' => 20,
      '#element_validate' => [
        [$this, 'validate'],
      ],
    ];

    $element['#theme_wrappers'] = ['container', 'form_element'];
    $element['#attributes']['class'][] = 'container-inline';
    $element['#attributes']['class'][] = 'student-uic-elements';
    $element['#attached']['library'][] = 'student_uic/student_uic';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($element, FormStateInterface $form_state) {
    $settings = $this->fieldDefinition->getFieldStorageDefinition()->getSettings();
    $element_value = $form_state->getValue($this->fieldDefinition->getName()) != NULL ? $form_state->getValue($this->fieldDefinition->getName())[0]['student_uic'] : NULL;

    if (!empty($element_value)) {
      if (!empty($settings['maximum_length'])) {
        if (strlen($element_value) > $settings['maximum_length']) {
          $form_state->setError($element, $this->t('Maximum character length for %field_name must be %length.',
          [
            '%field_name' => $this->fieldDefinition->getLabel(),
            '%length' => $settings['maximum_length'],
          ]));
        }
      }

      if (!empty($settings['minimum_length'])) {
        if (strlen($element_value) < $settings['minimum_length']) {
          $form_state->setError($element, $this->t('Minimum character length for %field_name must be %length.',
          [
            '%field_name' => $this->fieldDefinition->getLabel(),
            '%length' => $settings['minimum_length'],
          ]));
        }
      }

      if ($settings['pattern']['regex'] == 0) {
        if ($settings['pattern']['options']['numeric'] == 0) {
          if (ctype_digit($element_value)) {
            $form_state->setError($element, $this->t('The field %field_name cannot contain numerical characters.', ['%field_name' => $this->fieldDefinition->getLabel()]));
          }
        }

        if ($settings['pattern']['options']['alpha'] == 0) {
          if (preg_match('/\pL/', $element_value)) {
            $form_state->setError($element, $this->t('The field %field_name cannot contain alphabetical characters.', ['%field_name' => $this->fieldDefinition->getLabel()]));
          }
        }

        /*
         * @todo Work on the validation for special characters.
         * The one currently implemented doesn't work fully.
         */

        if ($settings['pattern']['options']['special'] == 0) {
          if (!ctype_alnum($element_value)) {
            $form_state->setError($element, $this->t('The field %field_name can only contain alphanumeric characters.', ['%field_name' => $this->fieldDefinition->getLabel()]));
          }
        }
        elseif ($settings['pattern']['options']['special'] == 'special') {
          if (!strpbrk($element_value, $settings['pattern']['allowed_characters'])) {
            $form_state->setError($element,
            $this->t('The field %field_name can only contain the following special characters: %allowed_characters.',
            [
              '%field_name' => $this->fieldDefinition->getLabel(),
              '%allowed_characters' => $settings['pattern']['allowed_characters'],
            ]));
          }
        }
      }
      else {
        if (!preg_match($settings['pattern']['regex_pattern'], $element_value)) {
          $form_state->setError($element, $this->t('The provided value for %field_name does not match the expected pattern: %pregmatch',
          [
            '%field_name' => $this->fieldDefinition->getLabel(),
            '%pregmatch' => $settings['pattern']['regex_pattern'],
          ]));
        }
      }
    }
  }

}
