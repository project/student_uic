<?php

namespace Drupal\student_uic\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'student_uic_default' formatter.
 *
 * @FieldFormatter(
 *   id = "student_uic_default",
 *   label = @Translation("Default"),
 *   field_types = {"student_uic"}
 * )
 */
class StudentUICDefaultFormatter extends FormatterBase {

  /**
   * The user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * Entity type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs an AddressDefaultFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
    $this->user = $this->entityTypeManager->getStorage('user')->load(\Drupal::currentUser()->id());
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // @see \Drupal\Core\Field\FormatterPluginManager::createInstance().
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'obfuscate' => FALSE,
      'obfuscation_level' => 1,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();

    $element['obfuscate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Obfuscate Student UIC'),
      '#default_value' => $settings['obfuscate'],
      '#description' => $this->t("Users with the permission 'view full student uic' will be exempt from this setting.<p>Obfuscated characters will be replaced with an asterisk (*).</p>"),
    ];

    $element['obfuscation_level'] = [
      '#type' => 'number',
      '#title' => 'Obfuscation Level',
      '#default_value' => $settings['obfuscation_level'],
      '#states' => [
        'required' => [
          ':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][obfuscate]"]' => ['checked' => TRUE],
        ],
        'visible' => [
          ':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][obfuscate]"]' => ['checked' => TRUE],
        ],
      ],
      '#description' => $this->t('How many characters to obfuscate.'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $summary[] = $this->t('Obfuscation: @obfuscation', ['@obfuscation' => $settings['obfuscate'] == 1 ? 'Yes' : 'No']);
    if ($settings['obfuscate'] == 1) {
      $summary[] = $this->t('Obfuscation Level: @level', ['@level' => $settings['obfuscation_level']]);
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $settings = $this->getSettings();

    $student_uic = NULL;

    foreach ($items as $delta => $item) {

      if ($item->student_uic) {
        if ($settings['obfuscate'] == 1) {

          // If the current user does not have permission to view
          // the full student uic, obfuscate it based on the fields
          // obfuscation settings.
          if (!$this->user->hasPermission('view full student uic')) {
            $x = 0;

            // Ensure that the obfuscation doesn't create additional padding.
            $level = $settings['obfuscation_level'];
            if ($level > strlen($item->student_uic)) {
              $level = strlen($item->student_uic);
            }

            while ($x <= strlen($item->student_uic)) {
              if ($x < $level) {
                $student_uic .= '*';
              }
              else {
                $student_uic .= substr($item->student_uic, $x, 1);
              }

              $x++;
            }
          }
          else {
            $student_uic = $item->student_uic;
          }
        }
        else {
          $student_uic = $item->student_uic;
        }

        $element[$delta]['student_uic'] = [
          '#type' => 'item',
          '#markup' => $student_uic,
        ];
      }

    }

    return $element;
  }

}
